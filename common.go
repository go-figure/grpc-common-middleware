package common

import (
	"context"

	"gitlab.com/go-figure/errors"
	middleware "gitlab.com/go-figure/grpc-middleware"
	"gitlab.com/go-figure/instrum"
	"gitlab.com/go-figure/logr"
)

// AddInstrumentation adds instrumentation tooling to the request
func AddInstrumentation(parentCtx context.Context) middleware.Middleware {
	return func(next middleware.Handler) middleware.Handler {
		return func(ctx context.Context) (err error) {
			// add logr.Sink to context
			sink := logr.SinkFromContext(parentCtx)
			ctx = logr.SinkContext(ctx, sink)

			// add sync.WaitGroup to context
			wg := instrum.WaitGroupFromContext(parentCtx)
			wg.Add(1)
			defer wg.Done()
			ctx = instrum.WaitGroupContext(ctx, wg)

			return next(ctx)
		}
	}
}

// CallID adds a unique UUID to the request context.
func CallID(next middleware.Handler) middleware.Handler {
	return func(ctx context.Context) (err error) {
		ctx, uid := instrum.UIDContext(ctx, "call", 10)
		_ = uid
		return next(ctx)
	}
}

// RecoverPanic recovers a panicking request and uses it as an error in the response.
func RecoverPanic(next middleware.Handler) middleware.Handler {
	return func(ctx context.Context) (err error) {
		defer errors.Recover(&err)

		err = next(ctx)
		return
	}
}

func Logging(next middleware.Handler) middleware.Handler {
	return func(ctx context.Context) error {
		method := middleware.ServerInfoFromContext(ctx).FullMethod

		ctx, job := logr.Task(ctx, method, nil)
		err := next(ctx)
		job.Done(err, nil)

		return err
	}
}
