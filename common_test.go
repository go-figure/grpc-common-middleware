package common

import (
	"context"
	"errors"
	"net"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	middleware "gitlab.com/go-figure/grpc-middleware"
	"gitlab.com/go-figure/grpc-middleware-common/test_proto"
	"gitlab.com/go-figure/instrum"
	"gitlab.com/go-figure/logr"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/test/bufconn"
)

type testKey string

var echoKey testKey = "echo"

type testServer struct {
	stop func()
}

func TestUnaryContextPassed(t *testing.T) {
	server, client := testServerClient(t, addTestValue)
	defer server.Stop()

	// Tell the server we want to get the "echoKey" value from its context
	resp, err := client.UnaryEcho(context.Background(), &test_proto.EchoRequest{})
	require.Nil(t, err)

	// Check that the server context has the value we added to it at the top
	require.Equal(t, "OK", resp.ValueFromContext)
}

func TestStreamContextPassed(t *testing.T) {
	server, client := testServerClient(t, addTestValue)
	defer server.Stop()

	// Tell the server we want to get the "echoKey" value from its context
	stream, err := client.StreamEcho(context.Background(), &test_proto.EchoRequest{})
	require.Nil(t, err)

	// Check that the server context has the value we added to it at the top
	resp, err := stream.Recv()
	require.Nil(t, err)
	err = stream.CloseSend()
	require.Nil(t, err)
	require.Equal(t, "OK", resp.ValueFromContext)
}

func TestCallID(t *testing.T) {
	server, client := testServerClient(t, CallID)
	defer server.Stop()

	resp, err := client.CallID(context.Background(), &test_proto.EchoRequest{})
	require.Nil(t, err)

	// Check that the server context has the value we added to it at the top
	require.NotEqual(t, "", resp.ValueFromContext)
}

func TestRecoverPanic(t *testing.T) {
	server, client := testServerClient(t, RecoverPanic)
	defer server.Stop()

	_, err := client.Panic(context.Background(), &test_proto.EmptyRequest{})
	st, ok := status.FromError(err)
	require.True(t, ok)
	require.Equal(t, "panic: recovered panic", st.Message())

}

func TestLogging(t *testing.T) {
	ctx := context.Background()
	vw := &VerifyWriter{}
	ctx = logr.SinkContext(ctx, logr.NewWriterSink(vw))

	server, client := testServerClient(t, AddInstrumentation(ctx), Logging)
	defer server.Stop()

	_, err := client.Empty(context.Background(), &test_proto.EmptyRequest{})
	require.Nil(t, err)

	require.True(t, vw.Wrote)
}

func addTestValue(next middleware.Handler) middleware.Handler {
	return func(ctx context.Context) error {
		ctx = context.WithValue(ctx, echoKey, "OK")
		return next(ctx)
	}
}

func testServerClient(t *testing.T, mids ...middleware.Middleware) (*testServer, test_proto.TestClient) {
	server := &testServer{}

	ln := bufconn.Listen(512 * 1024)

	// Make GRPC server and use our middleware
	grpcServer := grpc.NewServer(
		grpc.UnaryInterceptor(middleware.UnaryServerInterceptor(mids...)),
		grpc.StreamInterceptor(middleware.StreamServerInterceptor(mids...)),
	)
	test_proto.RegisterTestServer(grpcServer, server)

	wait := make(chan bool)
	go func() {
		wait <- true
		grpcServer.Serve(ln)
	}()
	<-wait

	server.stop = func() {
		grpcServer.Stop()
	}

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()
	conn, err := grpc.DialContext(ctx, "bufnet",
		grpc.WithDialer(func(string, time.Duration) (net.Conn, error) {
			return ln.Dial()
		}),
		grpc.WithInsecure(),
		grpc.WithBlock(),
	)
	require.Nil(t, err)
	client := test_proto.NewTestClient(conn)

	return server, client
}

var _ test_proto.TestServer = &testServer{}

func (s *testServer) Stop() {
	s.stop()
}

func (s *testServer) Empty(ctx context.Context, req *test_proto.EmptyRequest) (*test_proto.EmptyReply, error) {
	return &test_proto.EmptyReply{}, nil
}

func (s *testServer) UnaryEcho(ctx context.Context, req *test_proto.EchoRequest) (*test_proto.EchoReply, error) {
	val, ok := ctx.Value(echoKey).(string)
	if !ok {
		return nil, errors.New("no such value in context")
	}

	return &test_proto.EchoReply{ValueFromContext: val}, nil
}

func (s *testServer) StreamEcho(req *test_proto.EchoRequest, streamServer test_proto.Test_StreamEchoServer) error {
	ctx := streamServer.Context()
	val, ok := ctx.Value(echoKey).(string)
	if !ok {
		return errors.New("no such value in context")
	}

	return streamServer.Send(&test_proto.EchoReply{ValueFromContext: val})
}

func (s *testServer) CallID(ctx context.Context, req *test_proto.EchoRequest) (*test_proto.EchoReply, error) {
	return &test_proto.EchoReply{ValueFromContext: instrum.UIDFromContext(ctx, "call")}, nil
}

func (s *testServer) Panic(ctx context.Context, req *test_proto.EmptyRequest) (*test_proto.EmptyReply, error) {
	panic("recovered panic")
	return &test_proto.EmptyReply{}, nil
}

type VerifyWriter struct {
	Wrote bool
}

func (w *VerifyWriter) Write(p []byte) (n int, err error) {
	w.Wrote = true
	return
}
