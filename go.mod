module gitlab.com/go-figure/grpc-middleware-common

go 1.13

require (
	github.com/golang/protobuf v1.3.2
	github.com/stretchr/testify v1.4.0
	gitlab.com/go-figure/errors v0.0.0-20191217115925-b4009bc27b43
	gitlab.com/go-figure/grpc-middleware v0.0.0-20191216110303-7582a1a70751
	gitlab.com/go-figure/instrum v0.0.0-20200113094226-04f2c1f272d5
	gitlab.com/go-figure/logr v0.0.0-20171109172829-d6063063ab0e
	google.golang.org/grpc v1.25.1
)
